# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Impeller WCR

### How do I get set up? ###

Download all Arduino libraries and programs from downloads, Refer the circuit diagram and make connections, upload code and you are good to go. (Mechanical components can be obtained from Vatsal)
### Contribution guidelines ###

Mechanical part printing and assembly- Vatsal
Procurement of required parts as per specifications- Atul
Electronic assembly and code- Sayali
### Who do I talk to? ###

Sayali, Aniket - Electronics, Atul, Vatsal- Mechanical